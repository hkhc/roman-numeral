package io.hkhc.roman

class Roman(var value: String) {

    var digits = mapOf(
            'I' to 1,
            'V' to 5,
            'X' to 10,
            'L' to 50
    ).withDefault { Int.MAX_VALUE }

    // We don't need this if we don't do error check
    // mode 1 means it can occurs 3 times consecutively
    // mode 1 means it can be reducer of next mode 1 digit
    // mode 5 means it cannot occurs consecutively
    // mode 5 means it must be followed by digit with smaller value
    enum class Mode(var maxConsecutive: Int) {
        MODE1(3), MODE5(1)
    }

    var modes : Map<Char, Mode> = mapOf(
            'I' to Mode.MODE1,
            'V' to Mode.MODE5,
            'X' to Mode.MODE1,
            'L' to Mode.MODE5
    ).withDefault { Mode.MODE1 }

    fun toInt():Int {
        try {
            return toInt(' ', 1)
        }
        catch (e:Exception) {
            throw Exception("Invalid Numeral '${value}'")
        }
    }

    private fun toInt(lastDigit: Char, occurence: Int=1): Int {

        if (value.isEmpty()) return 0

        val lead = value[0]

        val leadNumeral = digits.getValue(lead)
        val lastNumeral = digits.getValue(lastDigit)
        return if (lastNumeral>=leadNumeral) {

            val newOccurence = if (lastNumeral == leadNumeral) occurence + 1 else 1
            if (newOccurence > modes.getValue(lead).maxConsecutive) throw Exception()

            leadNumeral + Roman(value.substring(1)).toInt(lead, newOccurence)

        }
        else {

            if (modes.getValue(lastDigit)!=Mode.MODE1) throw Exception()

            -lastNumeral +                  // undo the last addition
            (leadNumeral - lastNumeral) +   // and add new value
            Roman(value.substring(1)).toInt(lead)

        }

    }

}