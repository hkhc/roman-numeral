package io.hkhc.roman

import org.junit.Assert.assertEquals
import org.junit.Test

class RomanTest {

    @Test
    fun `test boundary case`() {

        assertEquals(1, Roman("I").toInt())
        assertEquals(2, Roman("II").toInt())
        assertEquals(3, Roman("III").toInt())
        assertEquals(4, Roman("IV").toInt())
        assertEquals(5, Roman("V").toInt())
        assertEquals(6, Roman("VI").toInt())
        assertEquals(7, Roman("VII").toInt())
        assertEquals(8, Roman("VIII").toInt())
        assertEquals(9, Roman("IX").toInt())
        assertEquals(10, Roman("X").toInt())

        assertEquals(11, Roman("XI").toInt())
        assertEquals(12, Roman("XII").toInt())
        assertEquals(13, Roman("XIII").toInt())
        assertEquals(14, Roman("XIV").toInt())
        assertEquals(15, Roman("XV").toInt())
        assertEquals(16, Roman("XVI").toInt())
        assertEquals(17, Roman("XVII").toInt())
        assertEquals(18, Roman("XVIII").toInt())
        assertEquals(19, Roman("XIX").toInt())
        assertEquals(20, Roman("XX").toInt())

        assertEquals(21, Roman("XXI").toInt())
        assertEquals(22, Roman("XXII").toInt())
        assertEquals(23, Roman("XXIII").toInt())
        assertEquals(24, Roman("XXIV").toInt())
        assertEquals(25, Roman("XXV").toInt())
        assertEquals(26, Roman("XXVI").toInt())
        assertEquals(27, Roman("XXVII").toInt())
        assertEquals(28, Roman("XXVIII").toInt())
        assertEquals(29, Roman("XXIX").toInt())
        assertEquals(30, Roman("XXX").toInt())

        assertEquals(31, Roman("XXXI").toInt())
        assertEquals(32, Roman("XXXII").toInt())
        assertEquals(33, Roman("XXXIII").toInt())
        assertEquals(34, Roman("XXXIV").toInt())
        assertEquals(35, Roman("XXXV").toInt())
        assertEquals(36, Roman("XXXVI").toInt())
        assertEquals(37, Roman("XXXVII").toInt())
        assertEquals(38, Roman("XXXVIII").toInt())
        assertEquals(39, Roman("XXXIX").toInt())
        assertEquals(40, Roman("XL").toInt())

        assertEquals(41, Roman("XLI").toInt())
        assertEquals(42, Roman("XLII").toInt())
        assertEquals(43, Roman("XLIII").toInt())
        assertEquals(44, Roman("XLIV").toInt())
        assertEquals(45, Roman("XLV").toInt())
        assertEquals(46, Roman("XLVI").toInt())
        assertEquals(47, Roman("XLVII").toInt())
        assertEquals(48, Roman("XLVIII").toInt())
        assertEquals(49, Roman("XLIX").toInt())
        assertEquals(50, Roman("L").toInt())
        
    }

    @Test(expected = Exception::class)
    fun `Test Invalid case 4`() = assertEquals(4, Roman("IIII").toInt())

    @Test(expected = Exception::class)
    fun `Test Invalid case 20`() = assertEquals(20, Roman("VV").toInt())

    @Test(expected = Exception::class)
    fun `Test Invalid case VX`() = assertEquals(15, Roman("VX").toInt())

}